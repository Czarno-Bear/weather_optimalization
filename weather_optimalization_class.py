"""

Napisano rzutowanie klas na zmienne.
Stworzono generator tupli

"""

import requests
import json


class WeatherForecast:
    def __init__(self, api_key):
        self.api_key = api_key
        self.history_dict = {}
        self.weather_dict = {}

    def __getitem__(self, item):
        return self.history_dict[str(item)]

    def __setitem__(self, key, value):
        self.history_dict[key] = value

    def __iter__(self):
        return iter(self.history_dict)

    def open_requests(self, querystring, take_date, time_file):
        url = "https://weatherapi-com.p.rapidapi.com/" + time_file

        headers = {
            'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
            'x-rapidapi-key': self.api_key
            }
        response = requests.request("GET", url,
                                    headers=headers, params=querystring)
        sl = {}
        take_date = str(take_date)
        self.weather_dict = response.json()
        for k in self.weather_dict["location"]:
            sl[k] = self.weather_dict["location"][k]
        for k in self.weather_dict["forecast"]["forecastday"][0]["day"]:
            sl[k] = self.weather_dict["forecast"]["forecastday"][0]["day"][k]
        for k in self.weather_dict["forecast"]["forecastday"][0]["astro"]:
            sl[k] = self.weather_dict["forecast"]["forecastday"][0]["astro"][k]
        self.history_dict[take_date] = sl
        return self.history_dict

    def read_file(self, take_date):
        with open('weather_history.json', 'r') as f:
            data = json.load(f)
            self.history_dict = data
            take_date = str(take_date)
            if take_date in data:
                return True

    def generator(self, take_date):
        for k, v in self.history_dict[str(take_date)].items():
            yield k, v

    def write_request(self):
        with open('weather_history.json', 'w') as f:
            json.dump(self.history_dict, f)
