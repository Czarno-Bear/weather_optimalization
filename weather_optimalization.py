"""

Program działa podobnie do poprzedniego z tym ze:


    if os.path.isfile('weather_history.json'):
        if wf.read_file(day) is True:

----- W tej sekcji zwracany jest generator tupli --------
            for line in wf.generator(day):
                print(line)
        else:

------ Tutaj nastepuje dodanie dnia jesli nie ma w pliku -------
            append_day()
    else:

------- A tutaj jesli nie ma w ogole pliku -----------
        append_day()

------- Tutaj nastepuje wyswietlenie zapisanych juz dat --------
    print("")
    for k in wf:
        print(k)

"""

import sys
import os
from datetime import date, timedelta
from weather_optimalization_class import WeatherForecast


def append_day():
    wf.open_requests(querystring, day, time_file)
    for key, value in wf[day].items():  # Uzycie wf[date]
        print(key, ": ", value)
    wf.write_request()


if len(sys.argv) < 2 or len(sys.argv) > 3:
    print("Podano zla ilosc argumentow")
else:

    api_key = sys.argv[1]

    d = date.today()
    t = timedelta(days=1)
    dt = d + t

    wf = WeatherForecast(api_key)

    day = date.fromisoformat(sys.argv[2]) if sys.argv[2:] else dt

    print("Pogoda na dzien: ", day)

    if day > d:
        time_file = "forecast.json"
        querystring = {"q": "Wagadugu", "days": "1", "dt": day, "lang": "pl"}
    else:
        time_file = "history.json"
        querystring = {"q": "Wagadugu", "dt": day, "lang": "pl"}

    if os.path.isfile('weather_history.json'):
        if wf.read_file(day) is True:
            for line in wf.generator(day):
                print(line)
        else:
            append_day()
    else:
        append_day()

    print("")
    for k in wf:
        print(k)
